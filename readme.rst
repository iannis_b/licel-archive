Overview
========

This package provides utilities to handle an archive of Licel binary files, typically used for
atmospheric lidar application.

Installation
============

The easiest way to install this module is from the python package index using ``pip``::

   pip install licel-archive

