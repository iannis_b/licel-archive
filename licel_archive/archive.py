import bisect
import codecs
import copy
import glob
import logging
import os
import re

import datetime
import matplotlib as mpl
import numpy as np
import pytz
from matplotlib import pyplot as plt

from atmospheric_lidar import licel, licelv2, raymetrics

logger = logging.getLogger(__name__)


class LicelArchive(object):

    measurement_classes = {'default': licel.LicelLidarMeasurement,
                           'scanning': raymetrics.ScanningLidarMeasurement,
                           'fixed': raymetrics.FixedPointingLidarMeasurement,
                           'licelv2': licelv2.LicelLidarMeasurementV2}


    def filenames_to_date_short(self, filenames):
        """ Convert the filename list to a datetime list.

        This method works with **short** format of Licel filenames (e.g. RM12A2810.341)

        Parameters
        ----------
        filenames : list
           A list of filenames to convert filename to convert.

        Returns
        -------
        datetimes : list
           A list of datetime object corresponding to the filenames.

        Notes
        -----
        Some ideas from here: https://stackoverflow.com/a/14163523
        """
        regex_str = r'\D{1,2}(?P<year>\d\d)(?P<month>[0-9,A,B,C])(?P<day>\d\d)(?P<hour>\d\d).(?P<minute>\d\d)(?P<second>\d)'
        pattern = re.compile(regex_str)

        datetimes = []
        for filename in filenames:
            match = pattern.match(filename)

            if not match:
                raise IOError("Filename {0} does not match short Licel format pattern.".format(filename))

            groups = match.groupdict()
            year = int(groups['year']) + 2000  # Convert to full year eg. 18->2018
            month = int(groups['month'], 16)  # Hexadeciaml format
            day = int(groups['day'])
            hour = int(groups['hour'])
            minute = int(groups['minute'])
            second = int(groups['second']) * 10 + 5  # Convert to approximate seconds e.g 1 to 15, 2 to 25 etc.

            d = datetime.datetime(year, month, day, hour, minute, second)
            d_utc = self._naive_to_utc(d)
            datetimes.append(d_utc)

        return datetimes

    def filenames_to_date_long(self, filenames):
        """ Convert the filename list to a datetime list.

        This method works with **long** format of Licel filenames (e.g. RM12A2810.341123)

        Parameters
        ----------
        filenames : list
           A list of filenames to convert filename to convert.

        Returns
        -------
        datetimes : list
           A list of datetime object corresponding to the filenames.

        Notes
        -----
        Some ideas from here: https://stackoverflow.com/a/14163523
        """
        regex_str = r'\D{1,2}(?P<year>\d\d)(?P<month>[0-9,A,B,C])(?P<day>\d\d)(?P<hour>\d\d).(?P<minute>\d\d)(?P<second>\d\d)(?P<microsecond>\d\d)'
        pattern = re.compile(regex_str)

        datetimes = []
        for filename in filenames:
            match = pattern.match(filename)

            if not match:
                raise IOError("Filename {0} does not match long Licel format pattern.".format(filename))

            groups = match.groupdict()
            year = int(groups['year']) + 2000  # Convert to full year eg. 18->2018
            month = int(groups['month'], 16)  # Hexadeciaml format
            day = int(groups['day'])
            hour = int(groups['hour'])
            minute = int(groups['minute'])
            second = int(groups['second'])
            microsecond = int(groups['microsecond']) * 10000  # Licel files give two decimal places of seconds.

            d = datetime.datetime(year, month, day, hour, minute, second, microsecond)
            d_utc = self._naive_to_utc(d)
            datetimes.append(d_utc)

        return datetimes

    def _find_files_in_archive(self):
        """ Populate the file list with all files found in the archive.

        The method also reads the approximate start time of each file, based on the filename.
        """
        search_pattern = os.path.join(self.base_path, self.file_pattern)

        full_paths = glob.glob(search_pattern)
        file_names = [os.path.basename(path) for path in full_paths]

        if len(full_paths) > 0:
            # Sort both based on file_names, see https://stackoverflow.com/a/9764364
            file_names, full_paths = zip(*sorted(zip(file_names, full_paths)))

            relative_paths = [os.path.relpath(path, self.base_path) for path in full_paths]

            if self.short_filenames:
                datetimes = self.filenames_to_date_short(file_names)
            else:
                datetimes = self.filenames_to_date_long(file_names)

            self.relative_paths = relative_paths
            self.stop_time_approximate = datetimes
        else:
            logger.warning("No files matching file_pattern were found.")

        self.full_paths = full_paths
        self.file_names = file_names

    def plot_channel(self, channel_name, figsize=(12, 5), vmin=None, vmax=None, minimum_length=8, date_labels=False,
                     zoom=[0, 12000, 0, None]):
        """ Plot a specific channel for a specif interval. """

        # Customize colorbar
        jet = copy.copy(mpl.cm.jet)
        jet.set_over('white')
        jet.set_under('black')
        jet.set_bad('black')

        fig = plt.figure(figsize=figsize)
        ax = plt.subplot(111)
        ax.set_facecolor((0.5, 0.5, 0.5))

        is_first = True

        for dataset in self.loaded_datasets:
            m = dataset['measurement']

            if (channel_name in m.channels.keys()) and (len(m.files) >= minimum_length):
                if is_first:
                    add_colorbar = True
                    is_first = False
                else:
                    add_colorbar = False

                c = m.channels[channel_name]
                c.draw_plot_new(ax, add_colorbar=add_colorbar, vmin=vmin, vmax=vmax, cmap=jet, date_labels=date_labels,
                                zoom=zoom)

        plt.tight_layout()
        plt.draw()
        plt.show()

        return fig, ax

    def _naive_to_utc(self, date):
        try:
            logger.debug('Creating timezone object %s' % self.timezone)
            timezone = pytz.timezone(self.timezone)
        except:
            raise ValueError("Cloud not create time zone object %s" % self.timezone)

        # According to pytz docs, timezones do not work with default datetime constructor.
        local_date = timezone.localize(date)

        # Only save UTC time.
        date_utc = local_date.astimezone(pytz.utc)

        return date_utc


class RawLicelArchive(LicelArchive):

    def __init__(self, base_path, file_pattern, file_version='default', short_filenames=True, timezone="UTC"):
        """ A Licel archive without any Datalog to describe indiviudal datasets.
        Parameters
        ----------
        base_path : str
           The base path of the archive.
        file_pattern : str
           A glob pattern for finding licel files in the archive (e.g. */*/*)
        file_version : str
           Version of licel files used. 'default': classic licel file, 'scanning': Scanning file with extra line.
        short_filenames : bool
           If True, short filenames are used (e.g. RM10A0210.351). If False, long filenames are used.
        """
        self.base_path = base_path
        self.file_pattern = file_pattern
        self.short_filenames = short_filenames
        self.file_version = file_version
        self.measurement_class = self.measurement_classes[file_version]
        self.timezone = timezone

        self._find_files_in_archive()
        self._get_exact_info()
        self.gaps = self._calculate_gaps(self.start_time_exact, self.stop_time_exact)

    def _get_exact_info(self):
        """ Get exact information, like start and stop time from the header of each file."""
        start_times = []
        stop_times = []
        durations = []
        number_of_channels = []

        for file_path in self.full_paths:
            # Read only file headers

            f = self.measurement_class.file_class(file_path, import_now=False, licel_timezone=self.timezone)
            f.import_header_only()

            # Append properties
            start_times.append(f.start_time)
            stop_times.append(f.stop_time)
            durations.append(f.duration())
            number_of_channels.append(f.number_of_datasets)

        self.start_time_exact = np.array(start_times)
        self.stop_time_exact = np.array(stop_times)
        self.durations = np.array(durations)
        self.number_of_channels = np.array(number_of_channels)

    @staticmethod
    def _calculate_gaps(start_times, stop_times):
        """ Calculate the time difference between consecutive files. .
        """
        gaps = []
        for start_next, stop_previous in zip(start_times[1:], stop_times[:-1]):
            dt = (start_next - stop_previous).total_seconds()
            gaps.append(dt)

        gaps = np.array(gaps)

        return gaps

    @staticmethod
    def get_continuous_idxs(gaps, max_gap_seconds=15):
        """ Calculate continuous indexes.

        Parameters
        ----------
        max_gap_seconds : int
           Maximum number of seconds between files to consider the two files continuous.

        Returns
        -------
        intervals : list
           A list of tuples, containing the start and stop indices for the continuous interval.
        """

        number_of_files = len(gaps) + 1
        large_gap_idxs = np.where(gaps > max_gap_seconds)[0]

        if len(large_gap_idxs) == 0:
            intervals = [(0, number_of_files), ]
        else:
            large_gap_idxs = large_gap_idxs + 1
            initial_idxs = np.insert(large_gap_idxs, 0, 0)
            final_idxs = np.append(large_gap_idxs, number_of_files)

            intervals = [(a, b) for a, b in zip(initial_idxs, final_idxs)]

        return intervals

    def load_measurements(self, start_time=None, stop_time=None, use_id_as_name=True):
        """ Load files covering a specific period. """

        if start_time is None:
            start_time = self.start_time_exact[0]

        if stop_time is None:
            stop_time = self.stop_time_exact[-1]

        # If naive datetimes are provided, assume UTC
        # TODO: Fix this based on provided timezone info
        if start_time.tzinfo is None:
            start_time = pytz.utc.localize(start_time)

        if stop_time.tzinfo is None:
            stop_time = pytz.utc.localize(stop_time)

        candidate_idx = (self.start_time_exact >= start_time) & (self.stop_time_exact <= stop_time)
        first_idx = np.argmax(candidate_idx)

        gaps = self._calculate_gaps(self.start_time_exact[candidate_idx], self.stop_time_exact[candidate_idx])
        intervals = self.get_continuous_idxs(gaps)

        # Split the files in continuous intervals
        datasets = []
        for interval_start, interval_stop in intervals:
            idx_min = interval_start + first_idx
            idx_max = interval_stop + first_idx
            files = self.full_paths[idx_min:idx_max]

            if self.file_version == 'licelv2':
                measurement = self.measurement_class(files[idx_min:idx_max])
            else:
                measurement = self.measurement_class(files, use_id_as_name=use_id_as_name)

            current_dataset = {'start_time': interval_start,
                               'stop_time': interval_stop,
                               'measurement': copy.copy(measurement)}
            datasets.append(current_dataset)

        self.loaded_datasets = datasets

    def plot_volume_depol(self, channel_name_p, channel_name_c, calibration, figsize=(12, 5), vmin=0, vmax=0.5, minimum_length=8, date_labels=False):
        """ Plot a specific channel for a specif interval. """

        # Customize colorbar
        jet = copy.copy(mpl.cm.jet)
        jet.set_over('white')
        jet.set_under('black')
        jet.set_bad('black')

        plt.figure(figsize=figsize)
        ax = plt.subplot(111)
        ax.set_facecolor((0.5, 0.5, 0.5))

        is_first = True

        for dataset in self.loaded_datasets:
            m = dataset['measurement']

            if (channel_name in m.channels.keys()) and (len(m.files) >= minimum_length):
                if is_first:
                    add_colorbar = True
                    is_first = False
                else:
                    add_colorbar = False

                c = m.channels[channel_name]
                c.draw_plot_new(ax, add_colorbar=add_colorbar, vmin=vmin, vmax=vmax, cmap=jet, date_labels=date_labels)

        plt.tight_layout()
        plt.draw()
        plt.show()

    @property
    def number_of_files(self):
        return len(self.full_paths)


class DatalogArchive(LicelArchive):

    def __init__(self, datalog_path, file_pattern, file_version='default', short_filenames=True,
                 timezone="UTC"):
        """ A Licel archive without any Datalog to describe indiviudal datasets.
        Parameters
        ----------
        datalog_path : str
           The path to the datalog file.
        file_pattern : str
           A glob pattern for finding licel files in the archive (e.g. */*/RM*)
        file_version : str
           Version of licel files used. 'default': classic licel file, 'scanning': Scanning file with extra line.
        short_filenames : bool
           If True, short filenames are used (e.g. RM10A0210.351). If False, long filenames are used.
        """
        if not os.path.isfile(datalog_path):
            raise IOError("Datalog path is not a file: {0}".format(datalog_path))

        self.datalog_path = datalog_path
        self.base_path = os.path.dirname(datalog_path)
        self.file_pattern = file_pattern
        self.short_filenames = short_filenames
        self.file_version = file_version
        self.measurement_class = self.measurement_classes[file_version]
        self.timezone = timezone

        self._find_files_in_archive()

        self.raw_entries = self._read_datalog_entries()
        self.datalog_records = self._get_datalog_records()

    def _read_datalog_entries(self):
        """ Read the datalog file. """
        with open(self.datalog_path, 'rb') as f:
            data = f.read()

        total_length = len(data)
        if total_length==0:
            raise IOError("Data log appears to be empty.")

        entries = []

        position = 0
        while position < total_length:
            length_str = data[position:position + 4]
            length = int(codecs.encode(length_str, 'hex'), 16)
            text = data[position + 4:position + 4 + length].decode()
            entries.append(text)
            position += 4 + length

        return entries

    def _get_datalog_records(self):
        """ Convert the list of raw datalog entries to measurement records. """
        # Split the list in groups of 4
        groups = [self.raw_entries[i:i + 4] for i in range(0, len(self.raw_entries), 4)]

        records = []
        for group in groups:
            record = {'user_name': group[0],
                      'location': group[1],
                      'start_date': self._datalog_str_to_date(group[2]),
                      'stop_date': self._datalog_str_to_date(group[3])}

            min_idx = bisect.bisect_left(self.stop_time_approximate, record['start_date'])
            max_idx = bisect.bisect_right(self.stop_time_approximate, record['stop_date'])
            record['files'] = self.full_paths[min_idx:max_idx]

            records.append(record)
        return records

    def print_records(self, min_idx=0, max_idx=None, date_fmt='%Y-%m-%d %H:%M:%S'):
        """ Print a list of records. """

        # Get max length of records
        user_length = max([len(record['user_name']) for record in self.datalog_records[min_idx:max_idx]])
        location_length = max([len(record['location']) for record in self.datalog_records[min_idx:max_idx]])

        print("No  {0:<{user_length}} {1:<{location_length}} Start               Stop".format('User', 'Location', user_length=user_length,
                                                                  location_length=location_length))
        ns = range(len(self.datalog_records))
        for n, record in zip( ns[min_idx:max_idx], self.datalog_records[min_idx:max_idx]):
            user = record['user_name']
            location = record['location']
            start = record['start_date'].strftime(date_fmt)
            stop = record['stop_date'].strftime(date_fmt)
            print("{0:<3} {1:<{user_length}} {2:<{location_length}} {3} {4}".format(n, user, location, start, stop,
                                                                                   user_length=user_length,
                                                                                   location_length=max(location_length, 8)
                                                                                   ))

    def load_measurements(self, record_idxs=None, use_id_as_name=True):
        """ Load files corresponding to the mentioned records.

        Parameters
        ----------
        record_idxs : None or int or list
           Indexes of datalog records. Can be a single index, or a list of indices.
        use_id_as_name : bool
           If True, the transient digitizer name (e.g. BT0) is used as a channel
           name. If False, a more descriptive name is used (e.g. '01064.o_an').
        """
        if type(record_idxs) is int:
            record_idxs = [record_idxs, ]  # Convert to list

        if record_idxs is None:
            record_idxs = range(len(self.datalog_records))

        # Split the files in continuous intervals
        datasets = []

        for idx in record_idxs:
            record = self.datalog_records[idx]

            if self.file_version == 'licelv2':
                measurement = self.measurement_class(record['files'])
            else:
                measurement = self.measurement_class(record['files'], use_id_as_name=use_id_as_name)

            current_dataset = {'idx': idx,
                               'start_time': record['start_date'],
                               'stop_time': record['stop_date'],
                               'measurement': copy.copy(measurement)}
            datasets.append(current_dataset)

        self.loaded_datasets = datasets

    def get_measurement(self, record_idx, use_id_as_name=True, idx_min=None, idx_max=None):
        """ Get the measurement object for a specific record idx.

        Parameters
        ----------
        record_idx : int
           Index of the datalog record to read.
        use_id_as_name : bool
           If True, the transient digitizer name (e.g. BT0) is used as a channel
           name. If False, a more descriptive name is used (e.g. '01064.o_an').
        idx_min : int or None
           Minimum index of file to use in measurement.
        idx_max : int or None
           Maximum index of file to use in measurement.
        """
        record = self.datalog_records[record_idx]
        files = sorted(record['files'])

        if self.file_version == 'licelv2':
            measurement = self.measurement_class(files[idx_min:idx_max])
        else:
            measurement = self.measurement_class(files[idx_min:idx_max], use_id_as_name=use_id_as_name)

        return measurement

    def _datalog_str_to_date(self, date_str):
        """ Convert a date string to a datetime object.

        Parameters
        ----------
        date_str : str
           A datetime string in the appropriate format e.g., 00:54:48:34 21/11/2016.

        Returns
        -------
        : datetime object
           The corresponding datetime object.
        """

        try:
            date = datetime.datetime.strptime(date_str, "%H:%M:%S:%f %d/%m/%Y")
        except ValueError:
            logger.debug('String {} is not in the following format: %H:%M:%S:%f %d/%m/%Y'.format(date_str))
            try:
                date = datetime.datetime.strptime(date_str, "%H:%M:%S %d/%m/%Y")
            except ValueError:
                logger.debug('String {} is not in the following format: %H:%M:%S %d/%m/%Y'.format(date_str))
                raise ValueError("Date string {} does not match any of the expected date formats.".format(date_str))

        date_utc = self._naive_to_utc(date)

        return date_utc

class ScanningArchive:
    def __init__(self, path, file_pattern='RM*', short_filenames=False):
        self.path = path
        self.file_pattern = file_pattern
        self.datalog_records = self.find_records()
        self.short_filenames = short_filenames

    @property
    def datalog_path(self):
        return self.path

    def find_records(self):
        rec_dirs = os.listdir(self.path)
        records = []
        for rec_dir in rec_dirs:
            meas_type = rec_dir.split('_')[0][0:2]
            dir_start_date = datetime.datetime.strptime(rec_dir.split('_')[1], "%y%m%d-%H%M%S")
            files = glob.glob(os.path.join(self.path, rec_dir, self.file_pattern))
            if len(files) == 0:  # TODO : Handle Empty records
                continue
            properties = self.find_scan_properties(files, meas_type)
            rec = dict(rec_dir=rec_dir, meas_type=meas_type, dir_start_date=dir_start_date, files=files)
            rec.update(properties)
            records.append(rec)
        return records

    @staticmethod
    def find_scan_properties(files, scan_type):
        files = [raymetrics.ScanningFile(file, import_now=False) for file in files]
        valid, start_date, stop_date, longitude, latitude, zenith_start, zenith_stop, zenith_step,  azimuth_start, \
            azimuth_stop, azimuth_step, length = ScanningArchive.verify_scan_files(files, scan_type)
        return dict(valid=valid, start_date=start_date, stop_date=stop_date, longitude=longitude, latitude=latitude,
                    zenith_start=zenith_start, zenith_stop=zenith_stop, zenith_step=zenith_step,
                    azimuth_start=azimuth_start, azimuth_stop=azimuth_stop, azimuth_step=azimuth_step, length=length)

    @staticmethod
    def verify_scan_files(files, scan_type):
        start_date = files[0].start_time
        stop_date = files[-1].stop_time
        longitude = latitude = zenith_start = zenith_stop = zenith_step = \
            azimuth_start = azimuth_stop = azimuth_step = length = valid = None
        try:
            longitude = list(set([file.longitude for file in files]))
            latitude = list(set([file.latitude for file in files]))
            if not len(longitude) == 1 and len(latitude) == 1:
                raise ValueError('Inconsistent coordinates in files!')
            longitude = longitude[0]
            latitude = latitude[0]

            zenith_start = list(set([file.zenith_start for file in files]))
            zenith_stop = list(set([file.zenith_stop for file in files]))
            zenith_step = list(set([file.zenith_step for file in files]))
            if not len(zenith_start) == 1 and len(zenith_stop) == 1 and len(zenith_step) == 1:
                raise ValueError('Inconsistent zenith parameters in files!')
            zenith_start = zenith_start[0]
            zenith_stop = zenith_stop[0]
            zenith_step = zenith_step[0]

            azimuth_start = list(set([file.azimuth_start for file in files]))
            azimuth_stop = list(set([file.azimuth_stop for file in files]))
            azimuth_step = list(set([file.azimuth_step for file in files]))
            if not len(azimuth_start) == 1 and len(azimuth_stop) == 1 and len(azimuth_step) == 1:
                raise ValueError('Inconsistent azimuth in files!')
            azimuth_start = azimuth_start[0]
            azimuth_stop = azimuth_stop[0]
            azimuth_step = azimuth_step[0]

            azimuth_start_raw = list(set([file.azimuth_start_raw for file in files]))
            azimuth_stop_raw = list(set([file.azimuth_stop_raw for file in files]))
            if not  len(azimuth_start_raw) == 1 and len(azimuth_stop_raw) == 1:
                raise ValueError('Inconsistent azimuth_raw parameters in files!')
            azimuth_start_raw = azimuth_start_raw[0]
            azimuth_stop_raw = azimuth_stop_raw[0]

            if scan_type == 'AS':
                if not azimuth_step > 0:
                    raise ValueError('Invalid azimuth step!')
                length = abs(azimuth_stop_raw - azimuth_start_raw) / abs(azimuth_step) + 1
                if not length == len(files):
                    raise ValueError('Inconsistent number of files!')
            elif scan_type == 'ZS':
                if not zenith_step > 0:
                    raise ValueError('Invalid zenith step!')
                length = abs(zenith_stop - zenith_start) / abs(zenith_step) + 1
                if not length == len(files):
                    raise ValueError('Inconsistent number of files!')
            else:
                length = len(files)

            valid = True
        except ValueError:
            valid = False
        finally:
            return valid, start_date, stop_date, longitude, latitude, zenith_start, zenith_stop, zenith_step, \
                   azimuth_start, azimuth_stop, azimuth_step, length
